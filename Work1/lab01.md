## Работа 1. Исследование гамма-коррекции
автор: Кошель Д.С.
дата: 25.04.2020

<!--https://gitlab.com/koshelmisis/kosheldaryamisis/-/tree/master/Work1 -->

### Задание
1. Сгенерировать серое тестовое изображение $I_1$ в виде прямоугольника размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
2. Применить  к изображению $I_1$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_1$.
3. Сгенерировать серое тестовое изображение $I_2$ в виде прямоугольника размером 768х60 пикселя со ступенчатым изменением яркости от черного к белому (от уровня 5 с шагом 10), одна градация серого занимает 30 пикселя по горизонтали.
4. Применить  к изображению $I_2$ гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение $G_2$.
5. Показать визуализацию результатов в виде одного изображения

### Результаты

![](lab01.png)
Рис. 1. Результаты работы программы (сверху вниз $I_1$, $G_1$, $I_2$, $G_2$)

### Текст программы

```cpp

#include <opencv2/opencv.hpp>
using namespace cv;



int main() {

	int h =60;

	int w = 786;

	Mat img(Mat::zeros(h, w, CV_8U));

	Mat imgres(Size(w, 2 * h), CV_8U);

	for (int col = 0; col < w; ++col) {

		for (int row = 0; row < h; ++row) {

			img.at<uchar>(Point(col, row)) = col / 3;

		}

	}

	img.copyTo(imgres(Rect(0, 0, w, h)));

	img.convertTo(img, CV_64F, 1.0 / 256);

	cv::pow(img, 2.2, img);

	img.convertTo(img, CV_8U, 256);

	img.copyTo(imgres(Rect(0, h, w, h)));






	imshow("work1.1", imgres);



	waitKey(0);


		Mat img2(Mat::zeros(h, w, CV_8U));

		Mat imgres2(Size(w, 2 * h), CV_8U);

		for (int col = 0; col < w; ++col) {

			for (int row = 0; row < h; ++row) {

				img2.at<uchar>(Point(col, row)) = 5 + col / 30*10;

			}

		}

		img2.copyTo(imgres2(Rect(0, 0, w, h)));

		img2.convertTo(img2, CV_64F, 1.0 / 256);

		cv::pow(img2, 2.3, img2);

		img2.convertTo(img2, CV_8U, 256);

		img2.copyTo(imgres2(Rect(0, h, w, h)));


		imshow("work1.2", imgres2);

		Mat save_arr[2] = { imgres, imgres2 };
		Mat save;
		vconcat(save_arr, 2, save);
		imwrite("lab01.png", save);

		
		waitKey(0);

}
```
