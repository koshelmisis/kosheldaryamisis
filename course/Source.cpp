// #include <opencv2/opencv.hpp>

#include <opencv2/objdetect.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

void display(Mat &im, Mat &bbox)

{
	int n = bbox.rows;
	for (int i = 0; i < n; i++)
	{
		line(im, Point2i(bbox.at<float>(i, 0), bbox.at<float>(i, 1)), Point2i(bbox.at<float>((i + 1) % n, 0), bbox.at<float>((i + 1) % n, 1)), Scalar(255, 0, 0), 3);
	}
	imshow("Result", im);

}

int main(int argc, char* argv[])

{
	// Read image
	Mat inputImage_simple, inputImage_rotated, inputImage_inphone, inputImage_inphand, inputImage_ingrass, inputImage_alot, inputImage_nine, inputImage_two, qr_proective_1;
	Mat qr_lupa, qr_crazy, qr_half, qr_x, qr_cube;
	if (argc > 1)
	{
		inputImage_simple = imread(argv[1]);
		inputImage_rotated = imread(argv[1]);
		inputImage_inphone = imread(argv[1]);
		inputImage_inphand = imread(argv[1]);
		inputImage_ingrass = imread(argv[1]);
		inputImage_alot = imread(argv[1]);
		inputImage_nine = imread(argv[1]);
		inputImage_two = imread(argv[1]);
	}
	else
	{
		inputImage_simple = imread("qr_simple.jpg");
		inputImage_rotated = imread("qr_rotated.jpg");
		inputImage_inphone = imread("qr_inphone.jpg");
		inputImage_inphand = imread("qr_inhand.jpg");
		inputImage_ingrass = imread("qr_ingrass.jpg");
		inputImage_alot = imread("qr_alot.jpg");
		inputImage_nine = imread("qr_nine.jpg");
		inputImage_two = imread("qr_two.jpg");
		qr_proective_1 = imread("qr_proective_1.jpg");
		qr_lupa = imread("qr_lupa.jpg");
		qr_crazy = imread("qr_crazy.jpg");
		qr_half = imread("qr_half.jpg");
		qr_x = imread("qr_x.jpg");
		qr_cube = imread("qr_cube.jpg");
	}

	QRCodeDetector qrDecoder;
	Mat bbox, rectifiedImage;
	std::string data_simple = qrDecoder.detectAndDecode(inputImage_simple, bbox, rectifiedImage);
	int intersection_simple_square, union_simple_square;
	if (data_simple.length() > 0)

	{
		display(inputImage_simple, bbox);
		cout << "inputImage_simple" << endl;
		cout << bbox << endl;
		imwrite("inputImage_simple.jpg", inputImage_simple);
		
		//inputImage_simple[38, 20; 210, 21; 210, 194; 38, 194]
		Rect box_etalon_simple = Rect(38, 20, 172, 174);
		Rect box_simple = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_simple = box_etalon_simple & box_simple;
		intersection_simple_square = intersection_simple.area();
		cout << "intersection_simple_square" << endl;
		cout << intersection_simple_square << endl;
		Rect union_simple = box_etalon_simple | box_simple;
		union_simple_square = union_simple.area();
		cout << "union_simple_square" << endl;
		cout << union_simple_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "inputImage_simple" << endl;
		cout << "QR Code not detected" << endl;
		//inputImage_simple[38, 20; 210, 21; 210, 194; 38, 194]
		Rect box_etalon_simple = Rect(38, 20, 172, 174);
		Rect box_simple = Rect(0, 0, 0, 0);
		Rect intersection_simple = box_etalon_simple & box_simple;
		intersection_simple_square = intersection_simple.area();
		cout << "intersection_simple_square" << endl;
		cout << intersection_simple_square << endl;
		Rect union_simple = box_etalon_simple | box_simple;
		union_simple_square = union_simple.area();
		cout << "union_simple_square" << endl;
		cout << union_simple_square << endl;
	}


	std::string data_rotated = qrDecoder.detectAndDecode(inputImage_rotated, bbox, rectifiedImage);
	int intersection_rotated_square, union_rotated_square;
	if (data_rotated.length()>0)

	{
		display(inputImage_rotated, bbox);
		cout << "inputImage_rotated" << endl;
		cout << bbox << endl;
		imwrite("inputImage_rotated.jpg", inputImage_rotated);
		//inputImage_rotated[171, 123; 707, 105; 756, 660; 175, 680]
		Rect box_etalon_rotated = Rect(171, 123, 536, 557);
		Rect box_rotated = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_rotated = box_etalon_rotated & box_rotated;
		intersection_rotated_square = intersection_rotated.area();
		cout << "intersection_rotated _square" << endl;
		cout << intersection_rotated_square << endl;
		Rect union_rotated = box_etalon_rotated | box_rotated;
		union_rotated_square = union_rotated.area();
		cout << "union_rotated _square" << endl;
		cout << union_rotated_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "inputImage_rotated" << endl;
		cout << "QR Code not detected" << endl;
		//inputImage_rotated[171, 123; 707, 105; 756, 660; 175, 680]
		Rect box_etalon_rotated = Rect(171, 123, 536, 557);
		Rect box_rotated = Rect(0, 0, 0, 0);
		Rect intersection_rotated = box_etalon_rotated & box_rotated;
		intersection_rotated_square = intersection_rotated.area();
		cout << "intersection_rotated _square" << endl;
		cout << intersection_rotated_square << endl;
		Rect union_rotated = box_etalon_rotated | box_rotated;
		union_rotated_square = union_rotated.area();
		cout << "union_rotated _square" << endl;
		cout << union_rotated_square << endl;
	}


	std::string data_inphone = qrDecoder.detectAndDecode(inputImage_inphone, bbox, rectifiedImage);
	int intersection_inphone_square, union_inphone_square;
	if (data_inphone.length()>0)

	{
		display(inputImage_inphone, bbox);
		cout << "inputImage_inphone" << endl;
		cout << bbox << endl;
		imwrite("inputImage_inphone.jpg", inputImage_inphone);
		//inputImage_inphone  [74, 104; 170, 104; 170, 200; 74, 200]
		Rect box_etalon_inphone = Rect(74, 104, 96, 96);
		Rect box_inphone = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_inphone = box_etalon_inphone & box_inphone;
		intersection_inphone_square = intersection_inphone.area();
		cout << "intersection_inphone_square" << endl;
		cout << intersection_inphone_square << endl;
		Rect union_inphone = box_etalon_inphone | box_inphone;
		union_inphone_square = union_inphone.area();
		cout << "union_inphone_square" << endl;
		cout << union_inphone_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "inputImage_inphone" << endl;
		cout << "QR Code not detected" << endl;
		//inputImage_inphone  [74, 104; 170, 104; 170, 200; 74, 200]
		Rect box_etalon_inphone = Rect(74, 104, 96, 96);
		Rect box_inphone = Rect(0, 0, 0, 0);
		Rect intersection_inphone = box_etalon_inphone & box_inphone;
		intersection_inphone_square = intersection_inphone.area();
		cout << "intersection_inphone_square" << endl;
		cout << intersection_inphone_square << endl;
		Rect union_inphone = box_etalon_inphone | box_inphone;
		union_inphone_square = union_inphone.area();
		cout << "union_inphone_square" << endl;
		cout << union_inphone_square << endl;
	}

	std::string data_inphand = qrDecoder.detectAndDecode(inputImage_inphand, bbox, rectifiedImage);
	int intersection_inphand_square, union_inphand_square;
	if (data_inphand.length()>0)

	{
		display(inputImage_inphand, bbox);
		cout << "inputImage_inphand" << endl;
		cout << bbox << endl;
		imwrite("inputImage_inphand.jpg", inputImage_inphand);
		//inputImage_inphand [291, 196; 467, 197; 468, 376; 290, 376]
		Rect box_etalon_inphand = Rect(291, 196, 176, 180);
		Rect box_inphand = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_inphand = box_etalon_inphand & box_inphand;
		intersection_inphand_square = intersection_inphand.area();
		cout << "intersection_inphand_square" << endl;
		cout << intersection_inphand_square << endl;
		Rect union_inphand = box_etalon_inphand | box_inphand;
		union_inphand_square = union_inphand.area();
		cout << "union_inphand_square" << endl;
		cout << union_inphand_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "inputImage_inphand" << endl;
		cout << "QR Code not detected" << endl;
		//inputImage_inphand [291, 196; 467, 197; 468, 376; 290, 376]
		Rect box_etalon_inphand = Rect(291, 196, 176, 180);
		Rect box_inphand = Rect(0, 0, 0, 0);
		Rect intersection_inphand = box_etalon_inphand & box_inphand;
		intersection_inphand_square = intersection_inphand.area();
		cout << "intersection_inphand_square" << endl;
		cout << intersection_inphand_square << endl;
		Rect union_inphand = box_etalon_inphand | box_inphand;
		union_inphand_square = union_inphand.area();
		cout << "union_inphand_square" << endl;
		cout << union_inphand_square << endl;
	}

	std::string data_inpgrass = qrDecoder.detectAndDecode(inputImage_ingrass, bbox, rectifiedImage);
	int intersection_ingrass_square, union_ingrass_square;
	if (data_inpgrass.length()>0)

	{
		display(inputImage_ingrass, bbox);
		cout << "inputImage_ingrass" << endl;
		cout << bbox << endl;
		imwrite("inputImage_ingrass.jpg", inputImage_ingrass);
		//qr_ingrass [122, 42; 216, 44; 216, 135; 120, 135]
		Rect box_etalon_ingrass = Rect(122, 42, 94, 93);
		Rect box_ingrass = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_ingrass = box_etalon_ingrass & box_ingrass;
		intersection_ingrass_square = intersection_ingrass.area();
		cout << "intersection_ingrass_square" << endl;
		cout << intersection_ingrass_square << endl;
		Rect union_ingrass = box_etalon_ingrass | box_ingrass;
		union_ingrass_square = union_ingrass.area();
		cout << "union_inphand_square" << endl;
		cout << union_ingrass_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "inputImage_ingrass" << endl;
		cout << "QR Code not detected" << endl;
		//qr_ingrass [122, 42; 216, 44; 216, 135; 120, 135]
		Rect box_etalon_ingrass = Rect(122, 42, 94, 93);
		Rect box_ingrass = Rect(0, 0, 0, 0);
		Rect intersection_ingrass = box_etalon_ingrass & box_ingrass;
		intersection_ingrass_square = intersection_ingrass.area();
		cout << "intersection_ingrass_square" << endl;
		cout << intersection_ingrass_square << endl;
		Rect union_ingrass = box_etalon_ingrass | box_ingrass;
		union_ingrass_square = union_ingrass.area();
		cout << "union_inphand_square" << endl;
		cout << union_ingrass_square << endl;
	}

	//std::string data_nine = qrDecoder.detectAndDecode(inputImage_nine, bbox, rectifiedImage);
	//if (data_nine.length()>0)

	//{
	//	display(inputImage_nine, bbox);
	//	cout << "inputImage_nine" << endl;
	//	cout << bbox << endl;
	//	imwrite("inputImage_nine.jpg", inputImage_nine);
	//	waitKey(0);
	//}
	//else
	//{
	//	cout << "inputImage_nine" << endl;
	//	cout << "QR Code not detected" << endl;
	//	waitKey(0);
	//}
	//std::string data_two = qrDecoder.detectAndDecode(inputImage_two, bbox, rectifiedImage);
	//if (data_two.length()>0)

	//{
	//	display(inputImage_two, bbox);
	//	cout << "inputImage_two" << endl;
	//	imwrite("inputImage_two.jpg", inputImage_two);
	//	waitKey(0);
	//}
	//else
	//{
	//	cout << "inputImage_two" << endl;
	//	cout << "QR Code not detected" << endl;
	//	waitKey(0);
	//}

	std::string data_proective_1 = qrDecoder.detectAndDecode(qr_proective_1, bbox, rectifiedImage);
	int intersection_proective_1_square, union_proective_1_square;
	if (data_proective_1.length()>0)

	{
		display(qr_proective_1, bbox);
		cout << "inputImage_qr_proective_1" << endl;
		cout << bbox << endl;
		imwrite("inputImage_qr_proective_1.jpg", qr_proective_1);
		//qr_proective_1 [582, 154; 831, 168; 735, 440; 484, 418]
		Rect box_etalon_proective_1 = Rect(582, 154, 249, 264);
		Rect box_proective_1 = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_proective_1 = box_etalon_proective_1 & box_proective_1;
		intersection_proective_1_square = intersection_proective_1.area();
		cout << "intersection_ingrass_square" << endl;
		cout << intersection_proective_1_square << endl;
		Rect union_proective_1 = box_etalon_proective_1 | box_proective_1;
		union_proective_1_square = union_proective_1.area();
		cout << "union_inphand_square" << endl;
		cout << union_proective_1_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "inputImage_qr_proective_1" << endl;
		cout << "inputImage_QR Code not detected" << endl;
		//qr_proective_1 [582, 154; 831, 168; 735, 440; 484, 418]
		Rect box_etalon_proective_1 = Rect(582, 154, 249, 264);
		Rect box_proective_1 = Rect(0, 0, 0, 0);
		Rect intersection_proective_1 = box_etalon_proective_1 & box_proective_1;
		intersection_proective_1_square = intersection_proective_1.area();
		cout << "intersection_ingrass_square" << endl;
		cout << intersection_proective_1_square << endl;
		Rect union_proective_1 = box_etalon_proective_1 | box_proective_1;
		union_proective_1_square = union_proective_1.area();
		cout << "union_inphand_square" << endl;
		cout << union_proective_1_square << endl;
		waitKey(0);
	}

	std::string data_lupa = qrDecoder.detectAndDecode(qr_lupa, bbox, rectifiedImage);
	int intersection_lupa_square, union_lupa_square;
	if (data_lupa.length()>0)

	{
		display(qr_lupa, bbox);
		cout << "qr_lupa" << endl;
		cout << bbox << endl;
		imwrite("inputImage_qr_lupa.jpg", qr_lupa);
		//qr_lupa [207, 120; 450, 120; 451, 363; 207, 363]
		Rect box_etalon_lupa = Rect(582, 154, 249, 264);
		Rect box_lupa = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_lupa = box_etalon_lupa & box_lupa;
		intersection_lupa_square = intersection_lupa.area();
		cout << "intersection_lupa_square" << endl;
		cout << intersection_lupa_square << endl;
		Rect union_lupa = box_etalon_lupa | box_lupa;
		union_lupa_square = union_lupa.area();
		cout << "union_inphand_square" << endl;
		cout << union_lupa_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "qr_lupa" << endl;
		cout << "inputImage_QR Code not detected" << endl;
		//qr_lupa [207, 120; 450, 120; 451, 363; 207, 363]
		Rect box_etalon_lupa = Rect(582, 154, 249, 264);
		Rect box_lupa = Rect(0, 0, 0, 0);
		Rect intersection_lupa = box_etalon_lupa & box_lupa;
		intersection_lupa_square = intersection_lupa.area();
		cout << "intersection_lupa_square" << endl;
		cout << intersection_lupa_square << endl;
		Rect union_lupa = box_etalon_lupa | box_lupa;
		union_lupa_square = union_lupa.area();
		cout << "union_inphand_square" << endl;
		cout << union_lupa_square << endl;
		waitKey(0);
	}

	std::string data_crazy = qrDecoder.detectAndDecode(qr_crazy, bbox, rectifiedImage);
	int intersection_crazy_square, union_crazy_square;
	if (data_crazy.length()>0)

	{
		display(qr_crazy, bbox);
		cout << "qr_crazy" << endl;
		cout << bbox << endl;
		imwrite("inputImage_qr_crazy.jpg", qr_crazy);
		//qr_crazy [372, 45; 703, 93; 626, 466; 195, 289]
		Rect box_etalon_crazy = Rect(372, 45, 331, 244);
		Rect box_crazy = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_crazy = box_etalon_crazy & box_crazy;
		intersection_crazy_square = intersection_crazy.area();
		cout << "intersection_crazy_square" << endl;
		cout << intersection_crazy_square << endl;
		Rect union_crazy = box_etalon_crazy | box_crazy;
		union_crazy_square = union_crazy.area();
		cout << "union_crazy_square" << endl;
		cout << union_crazy_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "qr_crazy" << endl;
		cout << "inputImage_QR Code not detected" << endl;
		//qr_crazy [372, 45; 703, 93; 626, 466; 195, 289]
		Rect box_etalon_crazy = Rect(372, 45, 331, 244);
		Rect box_crazy = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_crazy = box_etalon_crazy & box_crazy;
		intersection_crazy_square = intersection_crazy.area();
		cout << "intersection_crazy_square" << endl;
		cout << intersection_crazy_square << endl;
		Rect union_crazy = box_etalon_crazy | box_crazy;
		union_crazy_square = union_crazy.area();
		cout << "union_crazy_square" << endl;
		cout << union_crazy_square << endl;
		waitKey(0);
	}
	std::string data_half = qrDecoder.detectAndDecode(qr_half, bbox, rectifiedImage);
	int intersection_half_square, union_half_square;
	if (data_half.length()>0)

	{
		display(qr_half, bbox);
		cout << "qr_half" << endl;
		cout << bbox << endl;
		imwrite("inputImage_qr_half.jpg", qr_half);
		//qr_half [99, 49; 820, 49; 824, 853; 81, 853]
		Rect box_etalon_half = Rect(99, 49, 721, 804);
		Rect box_half = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_half = box_etalon_half & box_half;
		intersection_half_square = intersection_half.area();
		cout << "intersection_half_square" << endl;
		cout << intersection_half_square << endl;
		Rect union_half = box_etalon_half | box_half;
		union_half_square = union_half.area();
		cout << "union_half_square" << endl;
		cout << union_half_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "qr_half" << endl;
		cout << "inputImage_QR Code not detected" << endl;
		//qr_half [99, 49; 820, 49; 824, 853; 81, 853]
		Rect box_etalon_half = Rect(99, 49, 721, 804);
		Rect box_half = Rect(0, 0, 0, 0);
		Rect intersection_half = box_etalon_half & box_half;
		intersection_half_square = intersection_half.area();
		cout << "intersection_half_square" << endl;
		cout << intersection_half_square << endl;
		Rect union_half = box_etalon_half | box_half;
		union_half_square = union_half.area();
		cout << "union_half_square" << endl;
		cout << union_half_square << endl;
		waitKey(0);
	}
	std::string data_x = qrDecoder.detectAndDecode(qr_x, bbox, rectifiedImage);
	int intersection_x_square, union_x_square;
	if (data_x.length()>0)

	{
		display(qr_x, bbox);
		cout << "qr_x" << endl;
		cout << bbox << endl;
		imwrite("inputImage_qr_x.jpg", qr_x);
		//qr_x [155, 155; 1125, 154; 1125, 1125; 154, 1125]
		Rect box_etalon_x = Rect(155, 155, 970, 970);
		Rect box_x = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_x = box_etalon_x & box_x;
		intersection_x_square = intersection_x.area();
		cout << "intersection_x_square" << endl;
		cout << intersection_x_square << endl;
		Rect union_x = box_etalon_x | box_x;
		union_x_square = union_x.area();
		cout << "union_x_square" << endl;
		cout << union_x_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "qr_x" << endl;
		cout << "inputImage_QR Code not detected" << endl;
		//qr_x [155, 155; 1125, 154; 1125, 1125; 154, 1125]
		Rect box_etalon_x = Rect(155, 155, 970, 970);
		Rect box_x = Rect(0, 0, 0, 0);
		Rect intersection_x = box_etalon_x & box_x;
		intersection_x_square = intersection_x.area();
		cout << "intersection_x_square" << endl;
		cout << intersection_x_square << endl;
		Rect union_x = box_etalon_x | box_x;
		union_x_square = union_x.area();
		cout << "union_x_square" << endl;
		cout << union_x_square << endl;
		waitKey(0);
	}

	std::string data_cube = qrDecoder.detectAndDecode(qr_cube, bbox, rectifiedImage);
	int intersection_cube_square, union_cube_square;
	if (data_cube.length()>0)

	{
		display(qr_cube, bbox);
		cout << "qr_cube" << endl;
		cout << bbox << endl;
		imwrite("inputImage_qr_cube.jpg", qr_cube);
		//qr_cube [374, 57; 489, 162; 324, 300; 230, 179]
		Rect box_etalon_cube = Rect(374, 57, 115, 122);
		Rect box_cube = Rect(bbox.at<float>(0, 0), bbox.at<float>(0, 1), bbox.at<float>(1, 0) - bbox.at<float>(0, 0), bbox.at<float>(3, 1) - bbox.at<float>(0, 1));
		Rect intersection_cube = box_etalon_cube & box_cube;
		intersection_cube_square = intersection_cube.area();
		cout << "intersection_cube_square" << endl;
		cout << intersection_cube_square << endl;
		Rect union_cube = box_etalon_cube | box_cube;
		union_cube_square = union_cube.area();
		cout << "union_cube_square" << endl;
		cout << union_cube_square << endl;
		waitKey(0);
	}
	else
	{
		cout << "qr_cube" << endl;
		cout << "inputImage_QR Code not detected" << endl;
		//qr_cube [374, 57; 489, 162; 324, 300; 230, 179]
		Rect box_etalon_cube = Rect(374, 57, 115, 122);
		Rect box_cube = Rect(0, 0, 0, 0);
		Rect intersection_cube = box_etalon_cube & box_cube;
		intersection_cube_square = intersection_cube.area();
		cout << "intersection_cube_square" << endl;
		cout << intersection_cube_square << endl;
		Rect union_cube = box_etalon_cube | box_cube;
		union_cube_square = union_cube.area();
		cout << "union_cube_square" << endl;
		cout << union_cube_square << endl;
		waitKey(0);
	}

	double IoU = (intersection_simple_square + intersection_rotated_square + intersection_inphone_square + intersection_inphand_square + intersection_cube_square + intersection_ingrass_square +
		intersection_proective_1_square + intersection_lupa_square + intersection_crazy_square + intersection_half_square + intersection_x_square)*1.0 / (union_simple_square + union_rotated_square + union_inphone_square + union_inphand_square + union_cube_square + union_ingrass_square +
			union_proective_1_square + union_lupa_square + union_crazy_square + union_half_square + union_x_square)*1.0;
	cout << "IoU" << endl;
	cout << IoU << endl;
	waitKey(0);
}