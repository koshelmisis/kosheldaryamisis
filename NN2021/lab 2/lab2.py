# -*- coding: utf-8 -*-
"""lab1_20 (1) (1).ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1eBn9p8wYPSNTKxaEs__4hi8A8V2Br3UI
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import seaborn as sns
from sklearn.metrics import classification_report

df = pd.read_csv('glass.csv', header=0)
df.head()
X = df.iloc[:,0:8]
X.head()
Y = df.iloc[:,9]
Y.head()
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, random_state=0)

plt.figure(figsize = (10,10))
corr = df[df.columns[[i for i in range(10)]]].corr()
print('glass')
#sns.heatmap(corr, annot = True)

#df.identify_collinear(correlation_threshold = 0.98)

#sns.pairplot(df, hue='Type', palette='Set1')
def corr_df(x, corr_val):

    # Creates Correlation Matrix and Instantiates
    corr_matrix = x.corr()
    iters = range(len(corr_matrix.columns) - 1)
    drop_cols = []

    # Iterates through Correlation Matrix Table to find correlated columns
    for i in iters:
        for j in range(i):
            item = corr_matrix.iloc[j:(j+1), (i+1):(i+2)]
            col = item.columns
            row = item.index
            val = item.values
            if val >= corr_val:
                # Prints the correlated feature set and the corr val
                print(col.values[0], "|", row.values[0], "|", round(val[0][0], 2))
                drop_cols.append(i)

    drops = sorted(set(drop_cols))[::-1]

    # Drops the correlated columns
    for i in drops:
        col = x.iloc[:, (i+1):(i+2)].columns.values
        df = x.drop(col, axis=1)

    return df


dfcor = corr_df(df, 0.5)
Xcor = dfcor.iloc[:,0:8]
from sklearn.decomposition import PCA
pca = PCA(n_components=5)
principalComponents = pca.fit_transform(Xcor)
principalDf = pd.DataFrame(data = principalComponents
             , columns = ['principal component 1', 'principal component 2', 'principal component 3', 'principal component 4', 'principal component 5'])
principalDf
eig_vals, eig_vecs = np.linalg.eig(corr) 
#Make a list of (eigenvalue, eigenvector) tuples 

for i in range(len(eig_vals)):
  eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i])]
print(type(eig_pairs)) 
#Sort the (eigenvalue, eigenvector) tuples from high to low eig_pairs.sort() 
eig_pairs.reverse() 
print("\n",eig_pairs) 
#Visually confirm that the list is correctly sorted by decreasing eigenvalues 
print('\n\n\nEigenvalues in descending order:') 
for i in eig_pairs: 
    print(i[0])
tot = sum(eig_vals) 
print("\n",tot) 
for i in sorted(eig_vals, reverse=True):
 var_exp = [(i / tot)*100]
print("\n\n1. Variance Explained\n",var_exp) 
cum_var_exp = np.cumsum(var_exp) 
print("\n\n2. Cumulative Variance Explained\n",cum_var_exp) 
print("\n\n3. Percentage of variance the first two principal components each contain\n ",var_exp[0:2]) 
print("\n\n4. Percentage of variance the first two principal components together contain\n",sum(var_exp[0:2]))

print(df.columns)

#Критерий Кайзера
!pip install factor_analyzer
from factor_analyzer import FactorAnalyzer
fa = FactorAnalyzer(rotation=None)
fa.fit(X)
ev, v = fa.get_eigenvalues()
#for y in ev:
 # print("{0:.2f}".format(y))

print("Матрица факторной нагрузки")
fwv = pca.components_[:5]
fwv_df = pd.DataFrame(data={df.columns[i]:fwv[:,i] for i in range(ev.size)})
fwv_df

fwv_df.sum().abs().sort_values(ascending=False)

print("Матрица факторной нагрузки")
fwv = pca.components_[:5]
fwv_df = pd.DataFrame(data={df.columns[i]:fwv[:,i] for i in range(ev.size)})
fwv_df

fwv_df.sum().abs().sort_values(ascending=False)