
#include <windows.h>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

Mat GetGammaExpo()
{
	Mat result(1, 256, CV_8UC1);
	int y;
	for (int i = 0; i < 256; i++)
	{
		y = int(sin(i * 0.1227) * 128 + 128);
		if (y > 255) { y = 255; }
		if (y < 0) { y = 0; }
		result.at<uint8_t>(i) = y;
	}
	return result;
}

int main()
{
	Mat gray = imread("firefox.png", 0);
	namedWindow("Gray", 1);   
	imshow("Gray", gray);
	// ������������� ����������
	int histSize = 256;   
	float range[] = { 0, 255 };
	const float *ranges[] = { range };

	// C���������� �����������
	MatND hist;
	calcHist(&gray, 1, 0, Mat(), hist, 1, &histSize, ranges, true, false);

	// ����������� �����������
	int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);

	Mat histImage(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//
	namedWindow("Result", 1);    
	imshow("Result", histImage);
	imwrite("hist_firefox.png", histImage);
	int height = 256; //  
	int scale = 2; //  

	Mat res_lut;
	Mat lut = GetGammaExpo();
	Mat graph_img(Mat::zeros(height, 256 * scale, CV_8UC1));
	graph_img.setTo(255);
	for (int i = 0; i < 256 - 1; i++) {
		line(graph_img, Point(i * scale, height - lut.at<uint8_t>(0, i)), Point(i * scale + scale, height - lut.at<uint8_t>(0, i + 1)), Scalar::all(0), 1);
	}
	imshow("LUT graph", graph_img);
	imwrite("lut_graph.png", graph_img);
	LUT(gray, lut, res_lut);
	imshow("LUT", res_lut);
	imwrite("lut_firefox.png", res_lut);

// ����������� lut
	MatND hist2;
	calcHist(&res_lut, 1, 0, Mat(), hist2, 1, &histSize, ranges, true, false);
	Mat histImage2(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist2, hist2, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage2, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//
	namedWindow("Result2", 1);
	imshow("Result2", histImage2);
	Mat lut_arr[2] = { histImage,  histImage2 };
	Mat res_lut_graph;
	vconcat(lut_arr, 2, res_lut_graph);
	imshow("LUT histogram", res_lut_graph);
	imwrite("hist_lut_graph.png", res_lut_graph);


// clahe

	Ptr<CLAHE> clahe = cv::createCLAHE();
	Mat res_clahe1;
	clahe->apply(gray, res_clahe1);
	putText(res_clahe1, "8x8", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	clahe->setClipLimit(4);
	clahe->setTilesGridSize(Size(4, 4));
	Mat res_clahe2;
	clahe->apply(gray, res_clahe2);
	putText(res_clahe2, "4x4", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	clahe->setClipLimit(40);
	clahe->setTilesGridSize(Size(32, 32));
	Mat res_clahe3;
	clahe->apply(gray, res_clahe3);
	putText(res_clahe3, "32x32", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	imwrite("clahe.1.png", res_clahe1);
	imwrite("clahe.2.png", res_clahe2);
	imwrite("clahe.3.png", res_clahe3);

	Mat clache_fin_arr[4] = { gray, res_clahe1, res_clahe2, res_clahe3 };
	// ����������� 
	MatND hist3;
	calcHist(&res_clahe1, 1, 0, Mat(), hist3, 1, &histSize, ranges, true, false);
	Mat histImage3(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist3, hist3, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage3, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//

	Mat hist_fin_arr0[2] = { histImage, histImage3 };

	// ����������� 
	MatND hist4;
	calcHist(&res_clahe2, 1, 0, Mat(), hist4, 1, &histSize, ranges, true, false);
	Mat histImage4(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist4, hist4, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage4, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//
	// ����������� 
	MatND hist5;
	calcHist(&res_clahe3, 1, 0, Mat(), hist5, 1, &histSize, ranges, true, false);
	Mat histImage5(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist5, hist5, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage5, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//

	Mat hist_fin_arr1[2] = { histImage4, histImage5 };
	imwrite("hist_clahe1.png", hist_fin_arr0[1]);
	imwrite("hist_clahe2.png", hist_fin_arr1[0]);
	imwrite("hist_clahe3.png", hist_fin_arr1[1]);
	Mat clache_fin, hist_fin, hist_fin0, hist_fin1;
	hconcat(clache_fin_arr, 4, clache_fin);
	vconcat(hist_fin_arr0, 2, hist_fin0);
	vconcat(hist_fin_arr1, 2, hist_fin1);
	Mat hist_fin_arr2[2] = { hist_fin0, hist_fin1 };
	hconcat(hist_fin_arr2, 2, hist_fin);
	imshow("clache", clache_fin);

	// �����������

	Mat image = imread("firefox.png", CV_8UC1);
	Mat blackAndWhite;
	Mat blackAndWhite2;

	// ����������� �����������
	threshold(image, blackAndWhite, 0, 255, THRESH_OTSU);
	imshow("binarization_hist1", blackAndWhite);
	imwrite("binarization_hist1.png", blackAndWhite);
	Mat bin_arr[2] = { image,  blackAndWhite };
	Mat res_bin_graph;
	vconcat(bin_arr, 2, res_bin_graph);
	imwrite("res_bin_graph1.png", res_bin_graph);
	// ����������� �������
	adaptiveThreshold(image, blackAndWhite2, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, 9);
	imshow("binarization_hist2", blackAndWhite2);
	imwrite("binarization_hist2.png", blackAndWhite2);
	Mat bin_arr2[2] = { image,  blackAndWhite2 };
	Mat res_bin_graph2;
	vconcat(bin_arr2, 2, res_bin_graph2);
	imwrite("res_bin_graph2.png", res_bin_graph2);
	//��������������� �������
	Mat morf_bin;
	morphologyEx(res_bin_graph, morf_bin, MORPH_OPEN, Mat(), Point(-1, 1), 1, 0);
	imshow("morf_bin", morf_bin);
	Mat bin_arr3[2] = { image,  morf_bin };
	Mat res_morf;
	vconcat(bin_arr3, 2, res_morf);
	imwrite("res_morf.png", res_morf);

	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(3, 3),
		Point(1, 1));
	Mat res_mask_count;
	dilate(image, res_mask_count, element);
	erode(res_mask_count, res_mask_count, element);



	// �����

	Mat res_mask;
	Mat visualmask1, visualmask2, mask;
	res_mask_count.convertTo(mask, CV_32FC1, 1 / 255.0, 0.0);
	mask.convertTo(mask, CV_8UC1, 1);
	bitwise_and(image, res_mask_count, visualmask1, mask);

	Mat result(1, 256, CV_8UC1);
	int y;
	for (int i = 0; i < 256; i++)
	{
		y = -i + 255;
		if (y > 255) { y = 255; }
		if (y < 0) { y = 0; }
		result.at<uint8_t>(i) = y;
	}
	Mat inverse = result;

	LUT(res_mask_count, inverse, res_mask_count);
	res_mask_count.convertTo(mask, CV_32FC1, 1 / 255.0, 0.0);
	mask.convertTo(mask, CV_8UC1, 1);
	bitwise_and(image, res_mask_count, visualmask2, mask);
	Mat res9_arr[2] = { visualmask1, visualmask2 };
	hconcat(res9_arr, 2, res_mask);
	imwrite("mask.png", res_mask);

	waitKey(0);
	return 0;
}
