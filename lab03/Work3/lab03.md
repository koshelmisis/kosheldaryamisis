## Работа 3. Обработка изображения в градациях серого
автор: Кошель Д.С.
дата: 28.04.2020

<!-- url: https://gitlab.com/koshelmisis/kosheldaryamisis/-/tree/master/lab03 -->

### Задание
1. Подобрать и зачитать небольшое изображение в градациях серого.
2. Построить и нарисовать гистограмму распределения яркости пикселей исходного изображения.
3. Сгенерировать табличную функцию преобразования яркости. Построить график  табличной функции преобразования яркости.
4. Применить табличную функцию преобразования яркости к исходному изображению и получить , нарисовать гистограмму преобразованного изображения.
5. Применить CLAHE с тремя разными наборами параметров (визуализировать обработанные изображения  их гистограммы ).
6. Реализовать глобальный метод бинаризации (подобрать порог по гистограмме, применить пороговую бинаризацию). Визуализировать на одном изображении исходное и бинаризованное изображения.
7. Реализовать метод локальной бинаризации. Визуализировать на одном изображении исходное  и бинаризованное изображения.
8. Улучшить одну из бинаризаций путем применения морфологических фильтров. Визуализировать на одном изображении бинарное изображение до и после фильтрации.
9. Сделать визуализацию бинарной маски после морфологических фильтров поверх исходного изображения (могут помочь подсветка цветом и альфа-блендинг).


### Результаты

![](https://c.radikal.ru/c20/2004/06/59723171b674.png)
Рис. 1. Исходное полутоновое изображение 

![](https://b.radikal.ru/b35/2004/ff/40721e4d21f8.png)
Рис. 2. Гистограмма сходного полутонового изображения

![](https://b.radikal.ru/b26/2004/12/680923eff8f6.png)
Рис. 3. Визуализация функции преобразования 

![](https://d.radikal.ru/d39/2004/61/9b614d85a0f9.png)
Рис. 4.1. Таблично пребразованное изображение

![](https://a.radikal.ru/a36/2004/8d/f3d834dbbe47.png)
Рис. 4.2. Гистограмма таблично-пребразованного изображения 

![](https://a.radikal.ru/a16/2004/83/137d1b114042.png)
Рис. 5.1. Преобразование CLAHE с параметрами ...

![](https://b.radikal.ru/b15/2004/85/bc8c3551a304.png)
Рис. 5.2. Гистограмма 1

![](https://a.radikal.ru/a27/2004/57/1288cdd06ac2.png)
Рис. 5.3. Преобразование CLAHE 2 с параметрами ...

![](https://a.radikal.ru/a11/2004/f2/479f1397b656.png)
Рис. 5.4. Гистограмма 2

![](https://c.radikal.ru/c17/2004/cd/ceefea42b465.png)
Рис. 5.5. Преобразование CLAHE 3 с параметрами ...

![](https://a.radikal.ru/a38/2004/ff/9430916f72d9.png)
Рис. 5.6. Гистограмма 3

![](https://b.radikal.ru/b07/2004/4a/4475b5750e67.png)
Рис. 6. Изображение до и после глобальной бинаризации

![](https://d.radikal.ru/d41/2004/4f/ceafd558c07f.png)
Рис. 7. Изображение до и после локальной бинаризации

![](https://d.radikal.ru/d00/2004/f0/2658bb4ed830.png)
Рис. 8. До и после морфологической фильтрации 

![](https://d.radikal.ru/d07/2004/21/6e94c5515cf6.png)
Рис. 9. Визуализация маски 

### Текст программы

```cpp

#include <windows.h>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

Mat GetGammaExpo()
{
	Mat result(1, 256, CV_8UC1);
	int y;
	for (int i = 0; i < 256; i++)
	{
		y = int(sin(i * 0.1227) * 128 + 128);
		if (y > 255) { y = 255; }
		if (y < 0) { y = 0; }
		result.at<uint8_t>(i) = y;
	}
	return result;
}

int main()
{
	Mat gray = imread("firefox.png", 0);
	namedWindow("Gray", 1);   
	imshow("Gray", gray);
	// инициализация параметров
	int histSize = 256;   
	float range[] = { 0, 255 };
	const float *ranges[] = { range };

	// CВычисление гистограммы
	MatND hist;
	calcHist(&gray, 1, 0, Mat(), hist, 1, &histSize, ranges, true, false);

	// Отображение гистограммы
	int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);

	Mat histImage(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//
	namedWindow("Result", 1);    
	imshow("Result", histImage);
	imwrite("hist_firefox.png", histImage);
	int height = 256; //  
	int scale = 2; //  

	Mat res_lut;
	Mat lut = GetGammaExpo();
	Mat graph_img(Mat::zeros(height, 256 * scale, CV_8UC1));
	graph_img.setTo(255);
	for (int i = 0; i < 256 - 1; i++) {
		line(graph_img, Point(i * scale, height - lut.at<uint8_t>(0, i)), Point(i * scale + scale, height - lut.at<uint8_t>(0, i + 1)), Scalar::all(0), 1);
	}
	imshow("LUT graph", graph_img);
	imwrite("lut_graph.png", graph_img);
	LUT(gray, lut, res_lut);
	imshow("LUT", res_lut);
	imwrite("lut_firefox.png", res_lut);

// гистограмма lut
	MatND hist2;
	calcHist(&res_lut, 1, 0, Mat(), hist2, 1, &histSize, ranges, true, false);
	Mat histImage2(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist2, hist2, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage2, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//
	namedWindow("Result2", 1);
	imshow("Result2", histImage2);
	Mat lut_arr[2] = { histImage,  histImage2 };
	Mat res_lut_graph;
	vconcat(lut_arr, 2, res_lut_graph);
	imshow("LUT histogram", res_lut_graph);
	imwrite("hist_lut_graph.png", res_lut_graph);


// clahe

	Ptr<CLAHE> clahe = cv::createCLAHE();
	Mat res_clahe1;
	clahe->apply(gray, res_clahe1);
	putText(res_clahe1, "8x8", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	clahe->setClipLimit(4);
	clahe->setTilesGridSize(Size(4, 4));
	Mat res_clahe2;
	clahe->apply(gray, res_clahe2);
	putText(res_clahe2, "4x4", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	clahe->setClipLimit(40);
	clahe->setTilesGridSize(Size(32, 32));
	Mat res_clahe3;
	clahe->apply(gray, res_clahe3);
	putText(res_clahe3, "32x32", Point(res_clahe1.cols * 0.3, res_clahe1.rows * 0.1), FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(255, 0, 0));

	imwrite("clahe.1.png", res_clahe1);
	imwrite("clahe.2.png", res_clahe2);
	imwrite("clahe.3.png", res_clahe3);

	Mat clache_fin_arr[4] = { gray, res_clahe1, res_clahe2, res_clahe3 };
	// гистограмма 
	MatND hist3;
	calcHist(&res_clahe1, 1, 0, Mat(), hist3, 1, &histSize, ranges, true, false);
	Mat histImage3(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist3, hist3, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage3, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//

	Mat hist_fin_arr0[2] = { histImage, histImage3 };

	// гистограмма 
	MatND hist4;
	calcHist(&res_clahe2, 1, 0, Mat(), hist4, 1, &histSize, ranges, true, false);
	Mat histImage4(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist4, hist4, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage4, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//
	// гистограмма 
	MatND hist5;
	calcHist(&res_clahe3, 1, 0, Mat(), hist5, 1, &histSize, ranges, true, false);
	Mat histImage5(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(hist5, hist5, 0, histImage2.rows, NORM_MINMAX, -1, Mat());

	for (int i = 1; i < histSize; i++)
	{
		line(histImage5, Point(bin_w*(i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
	}
	//

	Mat hist_fin_arr1[2] = { histImage4, histImage5 };
	imwrite("hist_clahe1.png", hist_fin_arr0[1]);
	imwrite("hist_clahe2.png", hist_fin_arr1[0]);
	imwrite("hist_clahe3.png", hist_fin_arr1[1]);
	Mat clache_fin, hist_fin, hist_fin0, hist_fin1;
	hconcat(clache_fin_arr, 4, clache_fin);
	vconcat(hist_fin_arr0, 2, hist_fin0);
	vconcat(hist_fin_arr1, 2, hist_fin1);
	Mat hist_fin_arr2[2] = { hist_fin0, hist_fin1 };
	hconcat(hist_fin_arr2, 2, hist_fin);
	imshow("clache", clache_fin);

	// бинаризация

	Mat image = imread("firefox.png", CV_8UC1);
	Mat blackAndWhite;
	Mat blackAndWhite2;

	// бинаризация гистограммы
	threshold(image, blackAndWhite, 0, 255, THRESH_OTSU);
	imshow("binarization_hist1", blackAndWhite);
	imwrite("binarization_hist1.png", blackAndWhite);
	Mat bin_arr[2] = { image,  blackAndWhite };
	Mat res_bin_graph;
	vconcat(bin_arr, 2, res_bin_graph);
	imwrite("res_bin_graph1.png", res_bin_graph);
	// бинаризация среднее
	adaptiveThreshold(image, blackAndWhite2, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, 9);
	imshow("binarization_hist2", blackAndWhite2);
	imwrite("binarization_hist2.png", blackAndWhite2);
	Mat bin_arr2[2] = { image,  blackAndWhite2 };
	Mat res_bin_graph2;
	vconcat(bin_arr2, 2, res_bin_graph2);
	imwrite("res_bin_graph2.png", res_bin_graph2);
	//морфологические фильтры
	Mat morf_bin;
	morphologyEx(res_bin_graph, morf_bin, MORPH_OPEN, Mat(), Point(-1, 1), 1, 0);
	imshow("morf_bin", morf_bin);
	Mat bin_arr3[2] = { image,  morf_bin };
	Mat res_morf;
	vconcat(bin_arr3, 2, res_morf);
	imwrite("res_morf.png", res_morf);

	Mat element = getStructuringElement(MORPH_ELLIPSE,
		Size(3, 3),
		Point(1, 1));
	Mat res_mask_count;
	dilate(image, res_mask_count, element);
	erode(res_mask_count, res_mask_count, element);



	// маска

	Mat res_mask;
	Mat visualmask1, visualmask2, mask;
	res_mask_count.convertTo(mask, CV_32FC1, 1 / 255.0, 0.0);
	mask.convertTo(mask, CV_8UC1, 1);
	bitwise_and(image, res_mask_count, visualmask1, mask);

	Mat result(1, 256, CV_8UC1);
	int y;
	for (int i = 0; i < 256; i++)
	{
		y = -i + 255;
		if (y > 255) { y = 255; }
		if (y < 0) { y = 0; }
		result.at<uint8_t>(i) = y;
	}
	Mat inverse = result;

	LUT(res_mask_count, inverse, res_mask_count);
	res_mask_count.convertTo(mask, CV_32FC1, 1 / 255.0, 0.0);
	mask.convertTo(mask, CV_8UC1, 1);
	bitwise_and(image, res_mask_count, visualmask2, mask);
	Mat res9_arr[2] = { visualmask1, visualmask2 };
	hconcat(res9_arr, 2, res_mask);
	imwrite("mask.png", res_mask);

	waitKey(0);
	return 0;
}

```
