## Работа 2. Визуализация искажений jpeg-сжатия
автор: Кошель Д.С.
дата: 05.05.2020

url: https://gitlab.com/koshelmisis/kosheldaryamisis/-/tree/master/lab02

### Задание
Для исходного изображения (сохраненного без потерь) создать jpeg версии с двумя уровнями качества (например, 95 и 65). Вычислить и визуализировать на одной “мозаике” исходное изображение, результаты сжатия, поканальные и яркостные различия.

### Результаты

![](https://d.radikal.ru/d30/2005/65/4e9151a1fc8b.jpg)
Рис 1. Исходное изображение 65

![](https://c.radikal.ru/c08/2005/5f/d6bd516d05fa.jpg)
Рис 2. Исходное изображение 95

![](https://c.radikal.ru/c42/2005/ed/110a44270e9f.png)
Рис 2. Исходное изображение 

![](https://d.radikal.ru/d19/2005/0a/670e0edb94c0.png)
![](https://b.radikal.ru/b13/2005/ac/bd2634c8dbd3.png)
Рис 3. Результат

### Текст программы

```cpp

#include <windows.h>
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	//
	// Попоробавала взять изображение меньше 300*300
	//
	Mat im(imread("bob.png"));
	int height = 300;
	// задаем параметры сжатия
	vector<int> compression_params;
	compression_params.push_back(IMWRITE_JPEG_QUALITY);
	compression_params.push_back(95);
	// сжатие 95
	imwrite("95.jpg", im, compression_params);

	compression_params.pop_back();
	compression_params.push_back(65);
	// сжатие 65
	imwrite("65.jpg", im, compression_params);

	Mat im95(imread("95.jpg"));
	Mat im65(imread("65.jpg"));
	// вывод изображений
	imshow("95", im95);
	imshow("65", im95);

	//bright
	// три уровня яркостных различий
	Mat par[3];
	// пустые матрицы для промежуточных результатов
	Mat emp(Mat::zeros(im.size(), CV_8UC1));
	Mat emp95(Mat::zeros(im95.size(), CV_8UC1));
	Mat emp65(Mat::zeros(im95.size(), CV_8UC1));
	// split divides a multi-channel array into several single-channel arrays. 
	split(im, par);
	split(im95, par);
	split(im65, par);
	Mat matr_unite[4] = { emp.clone(), par[0].clone(), par[1].clone(), par[2].clone() };
	Mat matr_unite95[4] = { emp95.clone(), par[0].clone(), par[1].clone(), par[2].clone() };
	Mat matr_unite65[4] = { emp65.clone(), par[0].clone(), par[1].clone(), par[2].clone() };
	Mat channels;
	Mat channels95;
	Mat channels65;
	// Применяем горизонтальную конкатенацию к заданным матрицам.
	hconcat(matr_unite, 4, channels);
	hconcat(matr_unite95, 4, channels95);
	hconcat(matr_unite65, 4, channels65);
	// перевод цветового пространства
	cvtColor(channels, channels, COLOR_GRAY2BGR);
	cvtColor(channels95, channels95, COLOR_GRAY2BGR);
	cvtColor(channels65, channels65, COLOR_GRAY2BGR);
	// сохраняем результаты для каждого изображения
	Mat bright = channels;
	Mat bright_95 = channels95;
	Mat bright_65 = channels65;

	//color
	// сохраняем rgb для каждого изображения отдельно
	Mat blue = im.clone() & cv::Scalar(255, 0, 0);
	Mat blue95 = im95.clone() & cv::Scalar(255, 0, 0);
	Mat blue65 = im65.clone() & cv::Scalar(255, 0, 0);
	Mat green = im.clone() & cv::Scalar(0, 255, 0);
	Mat green95 = im95.clone() & cv::Scalar(0, 255, 0);
	Mat green65 = im65.clone() & cv::Scalar(0, 255, 0);
	Mat red = im.clone() & cv::Scalar(0, 0, 255);
	Mat red95 = im95.clone() & cv::Scalar(0, 0, 255);
	Mat red65 = im65.clone() & cv::Scalar(0, 0, 255);
	// объединяем результаты
	Mat matr_unite_color[4] = { im, green, red,  blue };
	Mat matr_unite_color95[4] = { im95, green95,red95, blue95 };
	Mat matr_unite_color65[4] = { im65, green65,red65, blue65 };
	Mat colors;
	Mat colors95;
	Mat colors65;
	// Применяем горизонтальную конкатенацию к заданным матрицам.
	hconcat(matr_unite_color, 4, colors);
	hconcat(matr_unite_color95, 4, colors95);
	hconcat(matr_unite_color65, 4, colors65);
	Mat colors_first = colors;
	Mat colors_95 = colors95;
	Mat colors_65 = colors65;

	//show matrix
	Mat matrix_b[3] = { bright, bright_95, bright_65 };
	Mat matrix_bright;
	// Применяем вертикальную конкатенацию к заданным матрицам.
	vconcat(matrix_b, 3, matrix_bright);
	Mat show_bright, show_color;
	// готовим матрицу для вывода
	//resize(matrix_bright, show_bright, Size(int(matrix_bright.cols * height / matrix_bright.rows), height), 0, 0, INTER_LINEAR);
	//imshow("brightness", show_bright);
	imshow("brightness", matrix_bright);
	Mat matrix_c[3] = { colors_first, colors_95, colors_65 };
	Mat matrix_color;
	vconcat(matrix_c, 3, matrix_color);
	//resize(matrix_color, show_color, Size(int(matrix_color.cols * height / matrix_color.rows), height), 0, 0, INTER_LINEAR);
	//imshow("color_chanels", show_color);
	imshow("color_chanels", matrix_color);
	Mat final[2] = { matrix_color, matrix_bright };
	Mat lab2;
	vconcat(final, 2, lab2);
	imwrite("lab02.png", lab2);
	// добавляем различия для каждого варианта сжатия от исходной
	// пеерводим в hsv, чтобы посчитать общую разницу
	Mat bright_differ_hsv[3];
	Mat bright_differ_hsv_95[3];
	Mat bright_differ_hsv_65[3];
	// перевод цветового пространства
	Mat hsv_im, hsv_im95, hsv_im65;
	cvtColor(im, hsv_im, COLOR_BGR2HSV);
	cvtColor(im95, hsv_im95, COLOR_BGR2HSV);
	cvtColor(im65, hsv_im65, COLOR_BGR2HSV);
	// split divides a multi-channel array into several single-channel arrays. 
	split(hsv_im, bright_differ_hsv);
	split(hsv_im95, bright_differ_hsv_95);
	split(hsv_im65, bright_differ_hsv_65);
	// считаем разность для каждого и домножаем на коэффициент, чтобы сдeлать результат более видимым
	Mat diff_all1 = abs(bright_differ_hsv[1] - bright_differ_hsv_95[1]) * 100;
	Mat diff_all2 = abs(bright_differ_hsv[1] - bright_differ_hsv_65[1]) * 100;
	// посчитаем поканальные различия
	Mat bright_differ[3];
	Mat bright_differ_95[3];
	Mat bright_differ_65[3];
	// split divides a multi-channel array into several single-channel arrays. 
	split(im, bright_differ);
	split(im95, bright_differ_95);
	split(im65, bright_differ_65);
// считаем разность для каждого и домножаем на коэффициент, чтобы сдeлать результат более видимым
	Mat diff_green1 = abs(bright_differ[1] - bright_differ_95[1]) * 100;
	Mat diff_green2 = abs(bright_differ[1] - bright_differ_65[1]) * 100;

	Mat diff_blue1 = abs(bright_differ[0] - bright_differ_95[0]) * 100;
	Mat diff_blue2 = abs(bright_differ[0] - bright_differ_65[0]) * 100;

	Mat diff_red1 = abs(bright_differ[2] - bright_differ_95[2]) * 100;
	Mat diff_red2 = abs(bright_differ[2] - bright_differ_65[2]) * 100;

	// склеиваем изображения
	Mat array95[4] = { diff_green1, diff_red1, diff_blue1 , diff_all1 };
	Mat array65[4] = { diff_green2, diff_red2, diff_blue2 , diff_all2 };
	Mat res1, res2, res;
	hconcat(array95, 4, res1);
	hconcat(array65, 4, res2);
	Mat arr[2] = { res1, res2 };
	vconcat(arr, 2, res);

	Mat res_show;
	//resize(res, res_show, Size(int(matrix_color.cols * height / matrix_color.rows), height), 0, 0, INTER_LINEAR);
	//imshow("lab02_2.png", res_show);
	imshow("lab02_2.png", res);
	imwrite("lab02_2.png", res);

	//Mat lab2_super;
	//Mat final_super[3] = { matrix_color, matrix_bright, res };
	//vconcat(final_super, 3, lab2_super);
	//imshow("lab02_super.png", lab2_super);
	//imwrite("lab02_super.png", lab2_super);

	waitKey(0);
	return 0;
}


```
